import {
  FlatList,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Image,
} from 'react-native';
import {styled} from 'nativewind';
import {useGetCharacterQuery} from '../services/rickandmorty';
import Header from '../components/header';
import {getEpisodes} from '../utils/utils';
const StyledText = styled(Text);
const StyledView = styled(View);
const StyledTouch = styled(TouchableOpacity);

// create a component
const Home = props => {
  const {data, error, isLoading} = useGetCharacterQuery();

  const itemAction = item => {
    try {
      let episodeNum = getEpisodes(item.item.episode.length);

      props.navigation.navigate('Details', {
        data: item,
        episodeNum: episodeNum,
      });
    } catch (error) {
      //console.log('Error', error);
    }
  };

  const renderItem = item => {
    return (
      <StyledTouch
        onPress={() => itemAction(item)}
        className="bg-[#3b3f44] w-4/5 self-center  rounded-md m-4">
        <Image
          source={{
            uri: item.item.image,
          }}
          className="w-full h-72 rounded-t-md"
          resizeMode="cover"
        />
        <StyledView className="m-4">
          <StyledText tw="font-bold text-white text-lg">
            {item.item.name}
          </StyledText>
          <StyledView className="flex-row items-center">
            <StyledView className="w-2 h-2 rounded-tr-full bg-red-400 rounded-lg" />
            <StyledText tw="font-bold text-white text-xs ml-1">
              {item.item.status}
              {' - '}
              {item.item.species}
            </StyledText>
          </StyledView>
          <StyledText tw="font-thin text-white text-xs mt-6">
            Last known location:
          </StyledText>
          <StyledText tw="font-bold text-white text-xs">
            {item.item.location.name}
          </StyledText>
          <StyledText tw="font-thin text-white text-xs mt-6">
            First seen in:
          </StyledText>
          <StyledText tw="font-bold text-white text-xs">
            {item.item.origin.name}
          </StyledText>
        </StyledView>
      </StyledTouch>
    );
  };

  return (
    <StyledView className="flex-1 items-center justify-center ">
      <Header />
      <StyledView className="justify-around items-center mx-24 my-10">
        <StyledText tw="font-black text-4xl  text-black text-center">
          The Rick and Morty API
        </StyledText>
      </StyledView>
      <StyledView className="flex-1 justify-center bg-[#292a34] w-full pt-6">
        {isLoading ? (
          <ActivityIndicator color={'white'} />
        ) : (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={data?.results}
            renderItem={renderItem}
          />
        )}
      </StyledView>
    </StyledView>
  );
};

//make this component available to the app
export default Home;
