import {
  FlatList,
  ActivityIndicator,
  ImageBackground,
  TouchableOpacity,
  Text,
  View,
  Image,
} from 'react-native';
import {styled} from 'nativewind';
import {useGetEpisodeQuery} from '../services/rickandmorty';
import imagePath from '../constatns/imagePath';

const StyledText = styled(Text);
const StyledView = styled(View);
const StyledTouch = styled(TouchableOpacity);

// create a component
const Details = props => {
  const {data, error, isLoading} = useGetEpisodeQuery(
    props?.route?.params?.episodeNum,
  );

  const renderItem = item => {
    return (
      <StyledView className="p-4 border-b border-gray-300">
        <StyledView className="flex-row w-full items-center">
          <StyledView className="w-2/5 flex-row ">
            <StyledText tw="font-bold text-black text-base ">
              {item.index + 1 + ' - '}
            </StyledText>
            <StyledView>
              <StyledText tw="font-bold text-black text-base ml-2">
                Name
              </StyledText>
              <StyledText tw="font-bold text-black text-base m-2">
                Air Date
              </StyledText>
            </StyledView>
          </StyledView>

          <StyledView className="w-3/5">
            <StyledText tw=" text-base ml-2">{item.item.name}</StyledText>
            <StyledText tw="text-base m-2">{item.item.air_date}</StyledText>
          </StyledView>
        </StyledView>
      </StyledView>
    );
  };

  return (
    <StyledView className="flex-1 bg-white">
      <ImageBackground
        source={{
          uri: props.route.params.data.item.image,
        }}
        className="w-full h-96 self-center"
        resizeMode="cover">
        <StyledTouch onPress={() => props.navigation.goBack()}>
          <Image source={imagePath.icBack} className="w-6 h-6 mt-14 ml-6" />
        </StyledTouch>
      </ImageBackground>
      <StyledView className="self-center mt-5 flex-row items-center">
        <StyledView className="w-2 h-2 rounded-tr-full bg-[#50d71e] rounded-lg" />
        <StyledText tw="font-bold text-green text-base ml-2">
          {props.route.params.data.item.name}
        </StyledText>
        <StyledText tw="font-bold text-[#50d71e] text-base ml-2">
          {'(' + props.route.params.data.item.status + ')'}
        </StyledText>
      </StyledView>
      <StyledView className="self-center mt-2 flex-row items-center">
        <StyledText tw="font-bold text-[#fec77d] text-base ml-2">
          {'( ' +
            props.route.params.data.item.species +
            ' - ' +
            props.route.params.data.item.gender +
            ' )'}
        </StyledText>
      </StyledView>

      <StyledView className="bg-[#dedfde] m-5 h-8 flex-row items-center">
        <StyledText tw="font-bold text-[#2d2d2d] text-base ml-2">
          Episode{' (' + props.route.params.data.item.episode.length + ')'}
        </StyledText>
      </StyledView>

      {isLoading ? (
        <ActivityIndicator color={'black'} />
      ) : (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          renderItem={renderItem}
        />
      )}
    </StyledView>
  );
};

//make this component available to the app
export default Details;
