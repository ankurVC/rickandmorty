export const episodeSortFn = (first, second) => {
  first.episode > second.episode ? 1 : -1;
};

export const getEpisodes = episodes => {
  let episodeNum = '';
  for (var i = 0; i <= episodes; i++) {
    if (i == 0) {
      episodeNum = episodeNum + i;
    } else {
      episodeNum = episodeNum + ',' + i;
    }
  }
  return episodeNum;
};
