// Need to use the React-specific entry point to import createApi
import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';

// Define a service using a base URL and expected endpoints
export const rickAndMortyApi = createApi({
  reducerPath: 'rickAndMorty',
  baseQuery: fetchBaseQuery({baseUrl: 'https://rickandmortyapi.com/api/'}),
  endpoints: builder => ({
    getCharacter: builder.query({
      query: character => `character`,
    }),
    getLocation: builder.query({
      query: location => `location${location}`,
    }),
    getEpisode: builder.query({
      query: episode => `episode/${episode}`,
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {useGetCharacterQuery, useGetLocationQuery, useGetEpisodeQuery} =
  rickAndMortyApi;
