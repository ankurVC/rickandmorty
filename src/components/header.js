import React from 'react';
import {View, TouchableOpacity, Image, Text, SafeAreaView} from 'react-native';
import {styled} from 'nativewind';
import imagePath from '../constatns/imagePath';

const StyledText = styled(Text);
const StyledView = styled(View);

const Header = props => {
  return (
    <SafeAreaView className="bg-[#ffffff]">
      <StyledView className="w-full mt-3">
        <StyledView className="flex-row w-full items-center p-3 border-b border-gray-300">
          <StyledView className="w-2/5">
            <Image source={imagePath.icUser} className="w-8 h-8 ml-2" />
          </StyledView>
          <StyledView className="w-3/5 justify-around items-center flex-row">
            <StyledText tw="font-bold text-black text-base">Docs</StyledText>
            <StyledText tw="font-bold text-black text-base">About</StyledText>
            <StyledView className="w-8 h-7 rounded-lg justify-around items-center bg-[#fa970e]">
              <Image
                source={imagePath.icHeart}
                className="w-3 h-3"
                resizeMode="contain"
              />
            </StyledView>
          </StyledView>
        </StyledView>
      </StyledView>
    </SafeAreaView>
  );
};
export default Header;
