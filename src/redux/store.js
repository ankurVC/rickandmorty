import {applyMiddleware} from 'redux';
import appReducer from './reducers';
import {configureStore} from '@reduxjs/toolkit';
import {rickAndMortyApi} from '../services/rickandmorty';
import {setupListeners} from '@reduxjs/toolkit/query';

export const store = configureStore({
  reducer: {
    // Add the generated reducer as a specific top-level slice
    [rickAndMortyApi.reducerPath]: rickAndMortyApi.reducer,
    reducer: appReducer,
  },
  // Adding the api middleware enables caching, invalidation, polling,
  // and other useful features of `rtk-query`.
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(rickAndMortyApi.middleware),
});

// optional, but required for refetchOnFocus/refetchOnReconnect behaviors
// see `setupListeners` docs - takes an optional callback as the 2nd arg for customization
setupListeners(store.dispatch);
